let http = require('http');
let fs = require('fs');
    var os = require("os");
// read 
// create 
// update 
// delete 
// rename 

http
.createServer((req,res)=> {
    //=========== read file ===================
    // fs.readFile('index.html', function(err, data){
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     if(err) console.log('Our error is: ', err);
    //     else res.write(data);
    //     return res.end();
    // })

    //================== Create file ==================
    // fs.appendFile();
    // fs.open();
    // fs.writeFile();

    // fs.appendFile('../bye.txt', "file has been created", function(err){
    //     if(err) console.log(err);
    //     console.log('file has been created');
    // })

    // fs.open('./a/abc.txt', 'w', function(err){
    //     if(err) console.log(err);
    //     console.log('file created');
    // })

    // fs.writeFile('filewrite.txt', 'hello all', function(err){
    //     if(err) throw err;
    //     console.log('file created');
    // })

    // delete file =============================
    // fs.unlink('test.docx', function(err){
    //     if(err) throw err;
    //     console.log('file has been deleted');
    // })

    // file rename 
    // fs.rename('./a/abc.txt', './a/xyz.txt', function(err) {
    //     if(err) throw err;
    //     console.log('renamed');
    // })

    console.log("Platform: " + os.platform());
    console.log("Architecture: " + JSON.stringify(os.cpus()));

})
.listen(3030);
console.log('server is running');
