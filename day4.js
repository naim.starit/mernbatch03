// var arr = [];
// var data = new Array(); 

// function Student(name, age) {
//     this.studentName = name;
//     this.age = age;
//     this.info = function info() {
//         const {studentName, age} = this;
//         console.log(studentName, age);
//     }
// }

// var stu1 = new Student("Next", 2);
// stu1.info()
// console.log(stu1);


// class Student {
//     constructor(name, age) {
//         this.studentName = name;
//         this.age = age;
//     }
//     info() {
//         console.log(this.studentName, this.age);
//     }
// }

// var stu1 = new Student("Next", 5);
// stu1.info()
// let num = 54786;
// let lastDigit = num % 10;
// function takeBreakfast() {
//     return new Promise((resolve, reject)=>{
//        if (lastDigit === 6) {
//             setTimeout(()=> {
//                  resolve("take breakfast");
//             }, 4000)
//        }
//        else {
//            reject('something wrong')
//        }
//     })
// }

function takeBreakfast() {
    return new Promise((resolve)=>{
            setTimeout(()=> {
                 resolve("take breakfast");
            }, 1000)
    })
}

function brashYourTeeth() {
    return new Promise((resolve)=> {
        setTimeout(()=>{
            resolve('brush your teeth')
        },3000)
    })
}

async function execute() {
  try {
    var res1 = await brashYourTeeth();
    console.log(res1);

    var res2 = await takeBreakfast();
    console.log(res2);
  }
  catch(err) {
      console.log(err);
  }
    // brashYourTeeth().then((res) => {
    //     console.log(res);
    // });

    // takeBreakfast()
    //   .then((res) => {
    //     console.log(res);
    //   })
}

execute();