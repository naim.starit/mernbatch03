let http = require("http");

// create server
http
  .createServer(function (req, res) {
    res.write("hello from another server");
    res.end();
  })
  .listen(3000);

console.log("server is running");

