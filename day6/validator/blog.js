const Joi = require('joi');

const addressSchema = Joi.object({
        division: Joi.string(),
        country: Joi.string()
    })

const blogValidator = Joi.object({
  title: Joi.string().min(5).max(150).required(),
  body: Joi.string().required(),
  author: Joi.string().regex(/^[a-z0-9]{24}$/),
  address: addressSchema,
});



module.exports = {
    blogValidator
}
