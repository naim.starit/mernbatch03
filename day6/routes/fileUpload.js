const express = require('express');
const router = express.Router();

const {fileUploader} = require('../controller/fileUpload');
const upload = require('../middleare/fileUpload');

router.post('/image-upload', upload.single('image'), fileUploader);

module.exports = router;