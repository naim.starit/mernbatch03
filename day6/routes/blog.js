const express = require("express");
const router = express.Router();

const { getAll, create, getOne, getById } = require("../controller/blog");

router.get("/all", getAll);
router.get("/find-one/:id", getOne);
router.get("/getbyid/:id", getById);
router.post("/create", create);

module.exports = router;
