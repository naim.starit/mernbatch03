const express = require("express");
const router = express.Router();

const {
  hello,
  allStudent,
  findById,
  findByIdandName,
  registration,
  login,
  forgotPassword,
  resetCodeCheck,
  resetPassword,
  tempDelete,
  restore,
  deleteStudent,
} = require("../controller/student");
const { isAuthenticated, permission } = require("../middleare");

//user role: student, teacher

router.get("/", hello);
router.get("/all", allStudent);
router.get("/byid/:id",isAuthenticated, permission(['student', 'teacher']), findById);
router.get("/byid/:id([0-9]{1})/byname/:name", findByIdandName);

router.post("/registration", registration);
router.post("/login", login);
router.post("/forgot-password", forgotPassword);
router.post("/resetcode-check", resetCodeCheck);
router.post("/reset-password", resetPassword);

router.put("/temp-delete/:id", tempDelete);
router.put("/restore/:id", restore);
router.delete("/delete/:id", deleteStudent);

module.exports = router;
