const express = require("express");
const router = express.Router();

const { allProduct, findProductByFilter} = require('../controller/product');

router.get("/all", allProduct);
router.get("/:brand", findProductByFilter);

module.exports = router;
