const Student = require("../model/student");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
// const secretKey = "lskrfoieure45467545474sdkfjiodhjhsdfjk";
const hello = (req, res) => {
  res.send("Hello Next topper");
};

const allStudent = async(req, res) => {
  // route, controller
  const students = await Student.find({isDeleted: false});
  if (students.length) res.status(200).send(students);
  else {
    res.status(400).send({ msg: "No student found" });
  }
};

const findById = async (req, res) => {
  const { id } = req.params;
  let student = studentData.filter((ele) => ele.id === parseInt(id));
  res.status(200).send(student[0]);
};

const findByIdandName = (req, res) => {
  const { id, name } = req.params;
  let student = studentData.filter(
    (ele) =>
      ele.id === parseInt(id) && ele.name.toLowerCase() === name.toLowerCase()
  );
  if (student.length) res.status(200).send(student[0]);
  else res.status(400).send({ msg: "No student found" });
};

const registration = async (req, res) => {
  const { password, confirmPassword } = req.body;
  if (password !== confirmPassword) {
    res.send({ msg: "password dose not match" });
  } else {
    bcrypt.hash(password, 10, async(err, hash) => {
      req.body.password = hash;
       delete req.body.confirmPassword;
       const student = new Student(req.body);
       const data = await student.save();
       res.send({ msg: "posted successfully", data });
    });
  }
};


const login = async(req, res)=> {
  const { mail, password } = req.body;
  let user = await Student.findOne({mail});
  if(user) {
    let isValid = await bcrypt.compare(password, user.password);
    if(isValid) {
      const data = {
        email: user.mail,
        role: "student"
      };
       const token = jwt.sign(data, process.env.SECRET_KEY, {expiresIn: '10h'});
       res.send({
         msg: 'Login successfull', 
         token
       })
    }
    else {
    res.send({
      msg: 'Password does not match'
    });
    }
  }
  else {
    res.send({msg: 'User not found with this email'})
  }
}


const forgotPassword = async (req, res) => {
  try {
    const {email} = req.body;
    const student = await Student.findOne({mail: email});
    if(!student) {
      res.status(400).send({
        message: 'User not found'
      })
    }
    else {
      const value1 = Math.floor(Math.random() * 8999) + 1000;
      const value2 = Math.floor(Math.random() * 5000) + 3000;
      const finalCode = '' + value1 + value2;
      await Student.findOneAndUpdate(
        { mail: email },
        { $set: { resetCode: finalCode} }
      );
      //=========================== Nodemailer =================

      //===========1st step==============
      let transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "theafterlife018@gmail.com",
          pass: "************"
        },
      });

      //===============2nd step===============
      let mailOption = {
        from: "theafterlife018@gmail.com",
        to: email, 
        subject: "Forgot password code",
        text: "Your code", 
        html: `<h3>Copy the code below</h3><br/><h1 style="color:green">Your code is: ${finalCode}</h1>`
      };

      //================3rd step=====================
      await transporter.sendMail(mailOption, function(err, data){
        if(err) {
          res.send({
            message: 'Email sending failed!!',
            error: err
          })
        }
        else {
          res.send({
            message: 'Code has been sent successfully'
          })
        }
      })
    }
  } catch (error) {
    res.send(error);
  }
}

const resetCodeCheck = async(req, res) => {
  try {
    const {resetCode} = req.body;
    let data = await Student.findOne({resetCode});
    if(data) {
         const tokenData = {
           email: data.mail
         };
         const token = jwt.sign(tokenData, process.env.SECRET_KEY, {
           expiresIn: "10h",
         });

         res.send({
           message: "Your token",
           token
         })
    }
    else {
      res.send({
        message: "Your reset code is invalid"
      })
    }
  } catch (error) {
    res.send(error);
  }
}

const resetPassword = async(req, res) => {
  try {
    const {newPassword, confirmNewPassword} = req.body;
    const {token} = req.query;
    if(newPassword !== confirmNewPassword) {
      res.send({
        message: "Password does not match"
      })
    }

    else {
      let data = jwt.verify(token, process.env.SECRET_KEY);   
      if(data) {
          bcrypt.hash(newPassword, 10, async function(err, hash) {
            let studentData = await Student.findOneAndUpdate(
              { mail: data.email },
              {
                $set: { password: hash },
              }
            );
            res.send({
              message: "Reset password done successfully",
              data: studentData,
            });
          })
      }
    }

  } catch (error) {
    res.send(error);
  }
}

const tempDelete = async(req, res) => {
  try {
    const id = req.params.id;
    await Student.findOneAndUpdate(
      {_id: id},
      {
        $set: {isDeleted: true}
      } 
    )

    return res.json({
      msg: "Student has been deleted successfully"
    })

  }
  catch(error) {
    res.send(error)
  }
}
const restore = async(req, res) => {
  try {
    const id = req.params.id;
    await Student.findOneAndUpdate(
      {_id: id},
      {
        $set: {isDeleted: false}
      } 
    )

    return res.json({
      msg: "Student has been restored successfully"
    })

  }
  catch(error) {
    res.send(error)
  }
}


const deleteStudent = async(req, res) => {
  try {
    const id = req.params.id;
    await Student.findOneAndRemove(
      {_id: id}
    )

    return res.json({
      msg: "Student has been deleted successfully",
    });
  } catch (error) {
    res.send(error);
  }
}

module.exports = {
  hello,
  allStudent,
  findByIdandName,
  findById,
  registration,
  login,
  forgotPassword,
  resetCodeCheck,
  resetPassword,
  tempDelete,
  restore,
  deleteStudent,
};