const Blog = require("../model/blog");
const Student = require("../model/student");
const { blogValidator } = require("../validator/blog");

const getAll =  async(req, res) => {
    try {
        const data = await Blog.find({isDeleted: false});
        if(data.length) {
            res.status(200).send({
                data
            })
        }
        else {
            res.status(200).send({
                msg: 'No data found',
                data
            })
        }
    }
    catch(error) {
        res.status(400).send({
          error,
        });
    }
}

const getOne =  async(req, res) => {
    try {
        const id = req.params.id;
        const data = await Blog.findOne({isDeleted: false, _id: id});
        if(data) {
            const authorInfo = await Student.findOne({
                _id: data.author,
            });
            data.author = authorInfo.firstName;
            res.status(200).send({
                data
            })
        }
        else {
            res.status(200).send({
                msg: 'No data found',
                data
            })
        }
    }
    catch(error) {
        res.status(400).send({
          error,
        });
    }
};
const getById =  async(req, res) => {
    try {
        const id = req.params.id;
        const data = await Blog.findOne({isDeleted: false, _id: id})
        .populate('author');
        if(data) {
            res.status(200).send({
                data
            })
        }
        else {
            res.status(200).send({
                msg: 'No data found',
                data
            })
        }
    }
    catch(error) {
        res.status(400).send({
          error,
        });
    }
};



const create =  async(req, res) => {
    try {
       const {error} = blogValidator.validate(req.body);
       if(error) {
           res.status(400).send({
               msg: "Validation error",
               error: error.details[0].message
           })
       }
       else {
            const blog = new Blog(req.body);
            const data = await blog.save();
             res.status(201).send({
               msg: "Blog created",
               data,
             });
       }
    }
    catch(error) {
        res.status(400).send({
            error
        })
    }
}

module.exports = {
    getAll, 
    create,
    getOne,
    getById
}