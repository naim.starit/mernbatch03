const Image = require('../model/image');
const fileUploader = async(req, res) => {
    try {
        if(req.file) {
            res.send({
                message: 'File uploaded successfully',
                fileDetailes: req.file
            })
        }
        else {
            res.send({
                message: 'Upload a valid image'
            })
        }
    } catch(err) {
        res.send({
            err
        })
    }
};
module.exports = {
    fileUploader
}