const express = require("express");
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const { logger } = require('./middleare');
const rateLimit = require('express-rate-limit');
require('dotenv').config();

// routes
const studentRoutes = require('./routes/student.js');
const productRoutes = require('./routes/product.js');
const blogRoutes = require('./routes/blog.js');
const uploadRoutes = require('./routes/fileUpload');

// app.use(morgan('tiny'));
// app.use(logger); // app level

const limiter = rateLimit({
  windowMs: 40 * 1000,
  max: 5,
  message: 'Too many request, please try some time later'
})

app.use(express.json());
app.use('/students', limiter, studentRoutes);
app.use('/products',logger, productRoutes);
app.use('/blog', blogRoutes);
app.use('/upload', uploadRoutes);

// 2 types of middleware 
// third party 
// own middleware

// use of middleware
// router level
// app level
app.get("*", (req, res) => {
  res.status(400).send("API not found");
});
// database connection 
mongoose.connect('mongodb://localhost:27017/mern03')
.then(()=> console.log('database connected'))
.catch((err)=> console.log(err))

const port = process.env.PORT || 4000;
app.listen(port, () => console.log("server is listening on port", port));
