const multer = require('multer');

const fileStorageEngine =  multer.diskStorage({
    destination: (req, res, cb)=> {
        cb(null, './images');
    },
    filename: (req, file, cb)=> {
        cb(null, Date.now()+'-'+ file.originalname)
    }
});
const maxSize = 15000000; // 15mb
const upload =  multer({
  storage: fileStorageEngine,
  limits: {
      fieldSize: maxSize
  }
});

module.exports = upload;