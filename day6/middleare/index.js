const jwt = require('jsonwebtoken');
const jwtDecode = require('jwt-decode');

const logger = (req, res, next) => {
    console.log('hello from logger middleware');
    next();
};

// const isAuthenticated = (userRole) => {
//    return (req, res, next) => {
//       let userName, pass;
//       (userName = "next2022"), (pass = "123456");

//       const { user, password, role } = req.query;
//       if (userName === user &&
//          pass === password && 
//          userRole.includes(role)
//          ) next();
//       else res.send("You are not an authenticated user");
//     };
// }

const isAuthenticated = (req, res, next) => {
    const token = req.header('Authorization');
    if(!token) {
        return res.status(401).send({
            message: 'You are a unauthorized user'
        })
    }
    var data = jwt.verify(token, process.env.SECRET_KEY);
    req.user = data;
    next();
};

const permission = (userRole) => {
    return function (req, res, next) {
        const token = req.header('Authorization');
        if(!token) {
        return res.status(401).send({
            message: 'You are a unauthorized user'
        });
        }
    
        try {
            const {role} = jwtDecode(token);
            let isIncluded = userRole.includes(role);
            if(isIncluded) next();
            else {
                next(res.status(401).send({
                    message: "You are not authorized user"
                }))
            }
        }
        catch(err) {
            next(res.status(401).send({
                    message: "You are not authorized user"
                }))
        }
    }
}
module.exports = {
    logger, 
    isAuthenticated,
    permission
};