const mongoose = require("mongoose");
const { Schema } = mongoose;

const blogSchema = new Schema({
    title: String, 
    body: String, 
    author: {
        type: Schema.Types.ObjectId,
        ref: 'student'
    },
    // comments: [
    //     {
    //         type: Schema.Types.ObjectId,
    //         ref: 'comment'
    //     }
    // ],
    isDeleted: {
        type: Boolean,
        default: false
    },
    isApproved: {
        type: Boolean, 
        default: false
    },
    address: {
        division: String,
        country: {
            type: String, 
            default: "Bangladesh"
        }
    }
});

module.exports = mongoose.model("blog", blogSchema);