const mongoose = require('mongoose');
const bcrypt = require("bcryptjs");
const { Schema } = mongoose;

const studentSchema = new Schema({
    firstName: String,
    lastName: {
        type: String
    },
    mail: {
        type: String, 
        required: true,
        trim: true, 
        unique: true
    },
    password: {
        type: String
    },
    address: {
        division: String,
        country: {
            type: String, 
            default: "Bangladesh"
        }
    },
    isDeleted: {
        type: Boolean, 
        default: false
    },
    resetCode: {
        type: String,
        default: ""
    },
});

studentSchema.pre('save', function(next) {
    var user = this;

    if(this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function(err, salt) {
            if(err) {
                return next(err)
            }
        bcrypt.hash(user.password, salt, function(err, hash){
            if(err) {
                return next(err)
            }
            if(hash) {
                user.password = hash;
            }
            next()
        })
    })
    }
    else {
        next();
    }
})
module.exports = mongoose.model('student', studentSchema);